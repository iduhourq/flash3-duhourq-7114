#Habiendo 'hardcodeado' en la ejecucionel nombre de archivo de 
#entrada, salida y largo de ventana
import sys 
import datetime as dt

Archivo= open(sys.argv[1]) 

ME=[] #Matriz vacia de entrada donde 'appendeo' los datos crudos   
                    
for listas in Archivo:
    listas= listas.strip()
    ME.append(listas.split(",")) #ya me da directamente una lista

for j in range(len(ME)):
    v=0
    while v<len(ME[j]):
        if ME[j][v] !="NA":
            if v==0:
                ME[j][v]= dt.datetime.strptime(ME[j][v], '%Y-%m-%dT%H:%M:%S')
            if v>=1:  
                ME[j][v]= float(ME[j][v]) 
        v+=1
#convierto con esta parte del codigo los strings a floats y timestamps    

ventana = int(sys.argv[3])  #Esta variable representa a mi ventana hardcodeada
columnas = len(ME[0]) 
MS = [] #Matriz de salida a donde appendeo promedios suavizados y tiempos(seg)
for k in range(len(ME)-ventana+1): #k es el contador que desliza la ventana
    # inicializar vector
    promedio = [0] * columnas #creo y agrego un array en 0 
    for l in range(ventana): #aca deslizo dentro de la ventana
        j=1
        while j<columnas:
             #Recorrer todos los elementos en cada fila de la ventana
            valor = ME[k+l][j]
            if valor == 'NA':
                promedio[j] = 'NA'                
            else:
                if promedio[j] != 'NA':
                    promedio[j] += round(valor/ventana, 2) 
            j+=1
#Recorro por columnas primero, despues filas y voy sumando los valores/col
#acumulando cada parte del promedio por vuelta. En caso del 'NA', lo consulto 
#antes de seguir con la cuenta.  
    ti = ME[k][0]
    tf = ME[k+ventana-1][0]
    t = (tf - ti).total_seconds()
    promedio[0] = t
    MS.append(promedio) 
    #en el elemento 0 agrego la resta de timestamps
def dos_decimales(valor_promedio):
    if valor_promedio != 'NA':
        return '{:.2f}'.format(valor_promedio)
    return valor_promedio
    
def truncar_promedios(promedios):
    return (','.join(list(map(dos_decimales, promedios))))   
#convierto a strings unidos los valores dentro de lista de listas

print(list(map(truncar_promedios, MS)))
#Checkpoint para ch
salida = open(sys.argv[2],'w')  #sys.argv[2] es el nombre ya 'hardcodeado'
print('\n'.join(list(map(truncar_promedios, MS))), file= salida)
#escribo en la salida los cada fila separada
salida.close()